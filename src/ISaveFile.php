<?php

namespace Phr\Certificator;

use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\AplContent;


/**
 * 
 * @final Phr\Certificator\SaveFile
 * @param string filepath
 * @param ConfigContent
 * 
 * PHP 8.2 or above
 * 
 * @category Authorisation encryption, keys, fingerprints
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 * 
 * @see SaveFile
 * 
 */
interface ISaveFile 
{   
    public const VERSION = '1.0.1';
    /**
     * @var const 
     * File variations
     */
    public const FILE_APL = 'SFAPL-051';

    public const FILE_SFCRY = 'SFCRY-000';

    public const FILE_CRYLIST = 'SFCRY-LI';

    public const CERT_SFCRT = 'SFCERT-00';

    public const CERT_SFCRT_SIGNED = 'SFCERT-SG';

    public const CERT_SFCRT_SH = 'SFCERT-SH';

    public const CERT_SFCRT_PU = 'SFCERT-PU';

    public const CERT_SFCRT_PR = 'SFCERT-PR';

    public const CERT_SSCRT_PU = 'SSCERT-PU';

    public const CERT_SSCRT_PR = 'SSCERT-PR';

    public const FILE_DBL = 'SFDBL-005';


    /**
     * @static
     * @method reads config files.
     * @param string filepath to cert
     * @return array of config parameters.
     */
    public static function read(string $_file_to_read, string|null $_key = null, string|null $_rsa_key = null): array|object;
    /**
     * @access public
     * @method creates config file
     * @param ConfigContent
     * @param string filename. If null filename is
     * random md5 hashed.
     */
    public function create(ConfigContent|AplContent $_confg_content, string $_file_name = null): void; 
    /**
     * @method creates certiricate base on type
     * @param object content to certificate
     * @param string|null filename. If null faliname is
     * random md5 enrypted.
     */
    public function createCertificate(object $_content, string|null $_file_name = null): void;
    /// DEPRECIATED ***
    public function createConfigFile(ConfigContent $_confg_content, string $_file_name = null): void;

}