<?php

namespace Phr\Certificator;

use Phr\Certificator\CertificatorBase\IHashVars;
use Phr\Certificator\Encry\HashAlgo as ALGO;


/**
 * 
 * @final Phr\Certificator\Encryption
 * @param string key
 * 
 * PHP 8.2 or above
 * 
 * @category Authorisation encryption, keys, fingerprints
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 * 
 * @see Encryption 53 00 000
 * 
 */
interface IEncryption 
{   
    public const VERSION = '1.0.1';
    /**
     * @static
     * @method sslPublicEncrypt
     * @param publicKey Public rsa key
     * @param data data to encrypt
     * Encypt data with public rsa key
     * @return string
     */
    public static function sslPublicEncrypt( string $_public_key, $_data_to_encrypt ): string|null ;
    /**
     * @static
     * @method sslPublicDecrypt
     * @param publicKey
     * @param data data to decrypt
     * Decrypt data with public key !
     * @return string
     */
    public static function sslPublicDecrypt( string $_public_key, $_data_to_decrypt ): string|null ; 
    /**
     * @static
     * @method sslPrivateEncrypt
     * @param privateKey
     * @param dataToEncrpyt
     * Encrypts data with private key
     * @return string
     */
    public static function sslPrivateEncrypt( string $_private_key, $_data_to_decrypt ): string|null ;
    /**
     * @static
     * @method sslPrivateDecrypt
     * @param privateKey
     * @param dataToDecrypt
     * Decrypts data with private key
     * @return sting
     */
    public static function sslPrivateDecrypt( string $_private_key, $_data_to_decrypt ): string|null ;
    /**
     * @method fernetEncrypt
     * @param messageToEncode
     * @return string
     */
    public function fernetEncrypt(string $_message_to_encode): string;
    /**
     * @method fernetEncrypt
     * @param fernetDecrypt
     * @return string
     */
    public function fernetDecrypt(string $_cypher_to_decode): string;
    /**
     * @static
     * @method generate 32 byts base64 encoded key
     * @return string key
     */
    public static function generate32bytsKey(): string;
    /**
     * @static
     * @method generates hash,
     * default hash algorith is MD5
     * @see IHashVars in CertificatorBase
     * @return string encoded hash
     */
    public static function hashIt( string|int $_content, ALGO $_hash_algo = ALGO::MD5 ): string; 
    /**
     * @method encrpts text in AES-128-CBC algorithm
     * @param string content to encrypt
     * @return string enrypted
     */
    public function sslEncrypt(string $_content_to_encrypt): string;
    /**
     * @method decrypts in AES-128-CBC algorithm
     * @param string content to decrypt
     * @return string|false result
     */
    public function sslDecrypt(string $_content_to_decrypt): string | false;
    /**
     * @method brakes EoId string and returns 
     * uuid hash.
     * @return false|string file hash
     */
    public static function fileEncode(string $_eoId, string $_expected_key = null): false| string;
    /**
     * @method encode bin to hex
     * @return string
     */
    public static function encodeHex($_content): string;
    /**
     * @method decode hex to bin
     * @return string
     */
    public static function decodeHex(string $_content): string;

    public static function sslEncryptOptions(string $plaintext, string $key, int $iv);
}