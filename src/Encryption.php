<?php

namespace Phr\Certificator;

use Phr\Certificator\CertificatorBase\EncryptionBase;
use Phr\Certificator\ShaEncryption\Encrypt;
use Phr\Certificator\ShaEncryption\Decrypt;
use Phr\Certificator\Encry\HashAlgo as ALGO;



final class Encryption extends EncryptionBase implements IEncryption
{   
    public static function sslPublicEncrypt( string $_public_key, $_data_to_encrypt ): string|null  
    {   
        openssl_public_encrypt($_data_to_encrypt, $encrypted, $_public_key);
        return self::baseEncode( $encrypted );
    }
    public static function sslPublicDecrypt( string $_public_key, $_data_to_decrypt ): string|null  
    {   
        openssl_public_decrypt( self::baseDecode( $_data_to_decrypt ), $decrypted, $_public_key);
        return $decrypted;
    }
    public static function sslPrivateEncrypt( string $_private_key, $_data_to_decrypt ): string|null  
    {   
        openssl_private_encrypt($_data_to_decrypt, $encrypted, $_private_key);
        return self::baseEncode( $encrypted );
    }
    public static function sslPrivateDecrypt( string $_private_key, $_data_to_decrypt ): string|null 
    {   
        openssl_private_decrypt( self::baseDecode( $_data_to_decrypt ), $decrpted, $_private_key);
        return $decrpted;
    }
    public function fernetEncrypt(string $_message_to_encode): string
    {
        $fernet = new Encry\Fernet($this->key);
        return $fernet->encode($_message_to_encode);
    }
    public function fernetDecrypt(string $_cypher_to_decode): string
    {
        $fernet = new Encry\Fernet($this->key);
        return $fernet->decode($_cypher_to_decode);
    }
    public static function generate32bytsKey(): string 
    {
        return Encry\Fernet::generateKey();
    }
    public static function hashIt( string|int $_content, ALGO $_hash_algo = ALGO::MD5 ): string 
    {
        return Encry\Hash::hashEncodeContent($_content, $_hash_algo);
    }
    public function sslEncrypt(string $_content_to_encrypt): string
    {
        $ssl = new Encry\SslEncryption($this->key);
        return $ssl->sslEncrypt($_content_to_encrypt);
    }
    public function sslDecrypt(string $_content_to_decrypt): string | false
    {
        $ssl = new Encry\SslEncryption($this->key);
        return $ssl->sslDecrypt($_content_to_decrypt);
    }
    public static function baseEncode($_content)
    {
        return base64_encode($_content);
    }
    public static function baseDecode($_content)
    {
        return base64_decode($_content);
    }
    public static function fileEncode(string $_eoId, string $_expected_key = null): false| string
    {   
        return (self::breakId($_eoId, $_expected_key) == null) ? false: md5(self::breakId($_eoId));
    }
    public static function encodeHex($_content): string
    {
        return bin2hex($_content);
    }
    public static function decodeHex(string $_content): string
    {   
        return hex2bin($_content);
    }
    public static function encryptAES(string $plaintext, string $key, int $iv): string
    {   
        $cipher = "aes-256-cbc";
        $ivlen = openssl_cipher_iv_length($cipher);
        return openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv);
    }
    public static function decryptAES(string $plaintext, string $key, int $iv): string
    {   
        $cipher = "aes-256-cbc";
        $ivlen = openssl_cipher_iv_length($cipher);
        return openssl_decrypt($plaintext, $cipher, $key, $options=0, $iv);
    } 
    public static function sslEncryptOptions(string $plaintext, string $key, int $iv)
    {   
        $cipher = "aes-256-cbc";
        $ivlen = openssl_cipher_iv_length($cipher);
        return openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv);
    } 
}