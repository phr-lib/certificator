<?php

namespace Phr\Certificator\CertificatorBase\Macro;

class Crips
{   
    /**
     * 
     * @access public
     * @method code
     * @var codeLenght
     * Generates random code. Default code lenght is 5
     * 
     */
    public static function code( int $_code_lenght = 5): int
    {   
        return (int)substr(rand(),0,$_code_lenght);
    }

    /**
     * @access public
     * @method generateKeyBlock
     * @var int lenght of the key | default to 5
     */
    public static function generateKeyBlock( int $keyLenght = 5): string 
    {   
        $GeneratedKeyBlock = '';

        for($i = 1; $i <= $keyLenght; $i++){
            
            $GeneratedKeyBlock .=  (string)self::code();
            if($i < $keyLenght) $GeneratedKeyBlock .=  Gradients::STING;

        }
        return $GeneratedKeyBlock;
    }

    /**
     * @method generateUniquId
     * @return uniquId 63deaece8c034 - 63deaece8c035 - 63deaece8c036 - 63deaece8c037 - 63deaece8c038
     */
    public static function generateUniquId(): string 
    {
        return (string)uniqid().
                 '-'. uniqid().
                 '-'. uniqid().
                 '-'. uniqid().
                 '-'. uniqid();
                
    }
    
    /**
     * 
     * @method newGuid
     * Creates new guid
     */
    public static function newGuid(): string 
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X'
        , mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479)
        , mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    /**
     * 
     * @method generateFingerPrint
     * @return string finger print 
     * @var int finger print lenght | default to 5 blocks
     * Example: P2y::O0s::F8h::C8y::X4k
     * 
     */
    public static function generateFingerPrint( int $fingerPrintLenght = 5): string
    {   
        $generateFingerPrint = '';
        for( $i = 1; $i <= $fingerPrintLenght; $i++ ){

            $generateFingerPrint .= self::fingerPrint();
                if( $i < $fingerPrintLenght )
                    $generateFingerPrint .= Gradients::STING;

        }
        return $generateFingerPrint;
    }

     /**
     * @method password
     * @return string password hash
     */
    public static function password( string $_unencrypted_password ): string 
    {
        return (string)password_hash($_unencrypted_password, PASSWORD_DEFAULT); 
    }

    /**
     * @method password_vertify
     * @return bool 
     */
    public static function password_vertify( string $_unencrypted_password, string $hash): bool 
    {
        return password_verify($_unencrypted_password, $hash);
    }
    
    /**
     * @method encrypt
     * @var string content
     * @var string|int vertifier code
     * @return string enryped string
     */
    public static function encrypt( string $unencrypted, string|int $vertifier): string 
    {
        return crypt($unencrypted, $vertifier);
    }

    /**
     * @method generateUniqueKeyId
     */
    public static function generateUniqueKeyId( string $_pfeFix = "", bool $_t = true ): string 
    {
        return uniqid($_pfeFix, $_t);
    }

    /**
     * 
     * @access private
     * @method fingerPrint
     */
    private static function fingerPrint()
    {
        return chr(rand(65,90)).rand(0,9).chr(rand(97,122));
    }
}