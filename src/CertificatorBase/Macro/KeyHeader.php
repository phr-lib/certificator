<?php

namespace Phr\Certificator\CertificatorBase\Macro;

class KeyHeader
{   
    private string $timestamp;

    public function __construct(int $_timestamp)
    {
        $this->timestamp = $_timestamp;
    }
    public function toString(): string
    {
        return(
            Gradients::KEY_START.
            $this->timestamp
        );
    }
}