<?php

namespace Phr\Certificator\CertificatorBase\Macro;

interface Gradients 
{
    public const KEY_START = 'Key:start::';

    public const ENVELOPE = '#:$';

    public const BLOCK = '#:';

    public const STING = '::';

    public const STING_SIGH = '::::';

    public const BREAK = '-';

    public const KEY_END = '::kEYenD';

    public const HEADER = '   =/file::';

    public const CERTIFICATE = 'CERTIFICATE';

    public const CERT_START = "----- START CERTIFICATE -----";

    public const CERT_END = "----- END CERTIFICATE -------";

    public const SIGNATURE = '@SIGNATURE::';

    public const COMMENT = '#';

    public const CONF_LINE = '=';

    public const HEADER_SEPARATOR = "##";

    public const COMA = ".";

    public const APL_LINE = "#";

    public const WHITE_APACE = " ";

    public const CLOSE_FILE = "/#/ - file::closed::";

    public const DBL_SPR = "&";

    public const DBL_VAR_SPR = "%";




}