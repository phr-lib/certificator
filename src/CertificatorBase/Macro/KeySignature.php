<?php

namespace Phr\Certificator\CertificatorBase\Macro;

class KeySignature
{   
    private string $timestamp;

    public function __construct(int $_timestamp)
    {
        $this->timestamp = $_timestamp;
    }

    public function toString(): string
    {
        return(
            date(DATE_ATOM,  $this->timestamp).
            Gradients::KEY_END
        );
    }
}