<?php

namespace Phr\Certificator\CertificatorBase\Macro;

use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;

class LineFormat 
{
    public static function format(string $_line = '', int $_length = 64, string $_chr = GR::APL_LINE)
    {
        $lineChars = (int)strlen($_line);
        $completeLenght = abs($lineChars-$_length); 
        $complete = "";
        for($i=0; $i<$completeLenght; $i++)
        {
            $complete .= $_chr;
        }
        return $_line . ' ' . $complete;
    }
    public static function line(int $_length = 64, string $_chr = GR::APL_LINE)
    {
        $complete = "";
        for($i=0; $i<$_length; $i++)
        {
            $complete .= '#';
        }
        return $complete;
    }
    public static function title(string $_title = '', int $_length = 64)
    {   
        $title = '# '.$_title;
        $lineChars = (int)strlen($title);
        $completeLenght = abs($lineChars-$_length)-2; 
        $complete = "";
        for($i=0; $i<$completeLenght; $i++)
        {
            $complete .= " ";
        }
        return $title . $complete . ' #' ;
    }
    public static function statement(array $_statement, int $_length = 64, string $_chr = GR::WHITE_APACE)
    {
        $completeLenght = abs(
            (int)strlen($_statement[0])
            +(int)strlen($_statement[1])
            -$_length+2); 
        $complete = "";
        for($i=0; $i<$completeLenght; $i++)
        {
            $complete .= $_chr;
        }
        return $_statement[0] . ' ' . $complete.' ' . $_statement[1];
    }
}