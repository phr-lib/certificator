<?php

namespace Phr\Certificator\CertificatorBase;

interface EncryVar 
{   
    public const SSL_SHA512 = "sha512";

    public const AES128CBD = 'AES-128-CBC';

    public const HMACOPT = 'sha256';   
}
