<?php

namespace Phr\Certificator\CertificatorBase; 

interface ICertKeyBase
{
    public const KEYEXT = 'key';
}

abstract class CertKeyBase extends Base implements ICertKeyBase
{   
    public static string $extention;

    public function __construct()
    {
        self::$extention = self::KEYEXT;
    }
}