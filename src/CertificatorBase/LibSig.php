<?php

namespace Phr\Certificator\CertificatorBase;

use Phr\Certificator\IEncryption;
use Phr\Certificator\IRsaGenerator;
use Phr\Certificator\CertKey\ICertKey;


readonly class LibSig 
{
    public string $version;

    public string $rsaGeneratorVersion;

    public string $keyCertVersion;

    public function __construct()
    {
        $this->version = IEncryption::VERSION;
        $this->rsaGeneratorVersion = IRsaGenerator::VERSION;
        $this->keyCertVersion = ICertKey::VERSION;
    }
    public function toString(): string 
    {
        return('Certificator version: ' . $this->version
                .'Rsa generator version: ' . $this->rsaGeneratorVersion
                .'KeyCert version: ' . $this->keyCertVersion
                );
    }
}