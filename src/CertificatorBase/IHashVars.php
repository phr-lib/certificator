<?php

namespace Phr\Certificator\CertificatorBase;

interface IHashVars 
{
    public const MD2 = "md2";

    public const MD4 = "mmd4d2";

    public const MD5 = "md5";

    public const SHA1 = "sha1";

    public const SHA256 = "sha256";

    public const SHA384 = "sha384";

    public const SHA512 = "sha512";

    public const ripemd128 = "ripemd128";

    public const ripemd160 = "ripemd160";

    public const ripemd256 = "ripemd256";

    public const ripemd320 = "ripemd320";

    public const whirlpool = "whirlpool";

    public const tiger128 = "tiger128,3";

    public const tiger192 = "tiger192,3";

    public const tiger160 = "tiger160,4";

    public const SNEFRU = "snefru";

    public const GOST = "gost";

    public const ADLER = "adler32";


}