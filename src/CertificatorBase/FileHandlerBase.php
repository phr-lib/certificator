<?php

namespace Phr\Certificator\CertificatorBase; 

use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\ISaveFile as SF;
use Phr\Certificator\CertificatorBase\Macro\Crips;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Certificator\FileHandler\FileVars as FV;
use Phr\Certificator\FileHandler\Errors as ERR;



/**
 * @abstract
 * 
 * 
 */
abstract class FileHandlerBase 
{   
    /**
     * @access protected
     * @var ConfigFileSettings settings!
     */
    protected static FileSettings|null $settings = null;

    protected static FV $FV;

    protected static string $dirPath;

    protected static string|null $createdFilename = null;

    protected static int $timeStamp;

    /**
     * @var static FH
     * File handler variablie [!]
     */
    protected static $FH;

    protected static string $fullFile;

    protected static string $fileVar = SF::FILE_SFCRY;

    /// CONSTRUCTOR ***
    public function __construct(
        string $_dir_path,
        FileSettings|null $_settings = null
    ){
        self::$dirPath = $_dir_path;
        self::$settings = $_settings;
        self::$timeStamp = time();
        self::$FV = $_settings->FV;
    }
    public static function createPath(): void
    {
        if(!is_dir( self::$dirPath ))
                    mkdir( self::$dirPath, 0777, true);
    }
    protected static function start()
    {
        self::$FH = fopen(self::$fullFile, "w");
    }
    protected static function fileExtention(string $_file_to_read): string
    {
        $e = explode('.', $_file_to_read);
        return array_pop($e);
    }
    /**
     * @method forms filename or gives it random md5
     * hash name. Forms file extention !
     * @param string|null filename. Default is set to null
     * @return string full fine name
     */
    protected static function file(string|null $_filename): string 
    {   
        $file = ($_filename == null) ? self::$createdFilename = md5(rand()): $_filename;
        return $file.GR::COMA.self::$FV->ext();
    }
    /**
     * @method reads file line by line
     * @return array of file lines
     */
    protected static function readFileByLine(string $_file_to_read) : array
    {   
        if(!$_file_to_read) throw new SaveFileError(ERR::E5304001);
        self::$fullFile = $_file_to_read;
        if(!file_exists(self::$fullFile))throw new SaveFileError(ERR::E5304002);

        $f = fopen($_file_to_read, "r", FILE_SKIP_EMPTY_LINES);
        $headers = [];
        $content = [];
        $lineCount = 1;
        $linesToPass = [1,2,3];
        while(!feof($f))
        {   
            if(!in_array($lineCount, $linesToPass))
            {   
                $line = fgets($f);
                if(strlen($line) > 5) array_push($content, $line);
                
            }else self::validateHeader(fgets($f), $lineCount);
            $lineCount++;  
        };
        fclose($f); 
        return $content;
    }
    protected static function readHeader( string $_header )
    {   
        $headerLine = explode("\n", $_header);
        for($i=0;$i<3;$i++)
        {
            self::validateHeader($headerLine[$i],$i+1);
        }
        
    }
    protected static function readSignature( string $_signature ): array
    {   
        $signatureParts = explode(GR::SIGNATURE, $_signature);
        return $signatureParts;
    }
    /**
     * @method validate header by its type,
     * Its iterates thrue 3 lines of headers.
     */
    protected static function validateHeader(string $_header_line, int $_line): void
    {         
        if($_line == 1)
        {   
            $fileVar = substr($_header_line, 2,9);
            self::$FV =  FileSettings::getFileVar($fileVar);
            self::$fileId = substr($_header_line, 13,13);
            self::$timeStamp = substr($_header_line, 28,10);

        }elseif($_line == 2)
        {   
            $headerSignature = str_replace("#","",$_header_line);
            self::validateSecureHeader($headerSignature);       
        }  
    }
    /**
     * @method validate header. 
     * @throws SaveFileError 5300001 023543
     */
    private static function validateSecureHeader(string $headerSignature): void
    {      
            if(filemtime(self::$fullFile) !== self::$timeStamp)
                throw new SaveFileError(ERR::E5304501);
            if(md5(self::$fileId.self::$timeStamp) !== trim($headerSignature))
                throw new SaveFileError(ERR::E5304502);
    }
    /**
     * @method complets header line.
     */
    protected static function comHeaderLine(string $_header_line): string
    {
        $c = strlen($_header_line);
        while($c < 64)
        {
            $_header_line .= '#';
            $c++;
        };
        return $_header_line;
    }
    /**
     * @method generates file headers ( .conf/.confg/.cert)
     * Frist line of header ( Most important ).
     * Reader first read fist 40 bytes to determinate 
     * reading algorith.
     * @return string first header line.
     */
    private static function generateFirstHeaderLine(): string 
    {   
        self::$fileId = $fileId = Crips::generateUniqueKeyId('', false);
        $line = self::$FV->code().'::'.$fileId.GR::HEADER_SEPARATOR.self::$timeStamp;
        return $line;
    }

    private static string $fileId;
    /**
     * @method generate second line of header base
     * on file variant 
     * @return string second header line!
     */
    private static function generateSecondHeaderLine(): string 
    {   
        return md5(self::$fileId.self::$timeStamp);
        
    }
    protected static function contentParse(string $_cert_content): string
    {
        return str_replace("\n",'',str_replace(" ",'',$_cert_content)); 
    }
    /**
     * @method write first three lines of file
     * with file header.
     */
    protected static function writeHeader()
    {   
        $line_1 =   self::generateFirstHeaderLine();
        $line_2 =   self::generateSecondHeaderLine();

        $line_3 = "";
        if(self::$settings != null) $line_3 .= self::$settings->applicationId . GR::HEADER_SEPARATOR ;

        fwrite(self::$FH, self::comHeaderLine(GR::HEADER_SEPARATOR.$line_1). PHP_EOL);
        fwrite(self::$FH,self::comHeaderLine(GR::HEADER_SEPARATOR.$line_2). PHP_EOL);
        fwrite(self::$FH,self::comHeaderLine(GR::HEADER_SEPARATOR.$line_3.date("Y-m-d.(H:i:s)",self::$timeStamp)). PHP_EOL);
    }
    
}