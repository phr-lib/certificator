<?php

namespace Phr\Certificator\CertificatorBase; 

use Phr\Certificator\CertificatorBase\Macro\Gradients;

/**
 * @abstract 
 * 
 * @see CertKey folder
 * 
 * Model for keys and certificate
 * 
 */
abstract class CertKeyModel 
{   
    /**
     * @access public
     * Constants for file extentions 
     * [key]
     * 
     */
    public const KEYEXT = "key";

    /**
     * @access protected
     * @var string path to file source - 
     * saveing file to directory
     */
    protected string $keyPath;

    protected int $timestamp;

    // CONSTRUCTOR ***
    public function __construct(string $_key_path)
    {
        $this->keyPath = $_key_path;
    }
    /**
     * @access protected
     * @method creates new timestamp !
     */
    protected function newTimeStamp(): void
    {
        $this->timestamp =  (int)time();
    }
    /**
     * @method forms body of the key
     * All key body format
     */
    protected function formBody(string $_body_to_form): void
    {
        self::$body =   Gradients::ENVELOPE.
                        $_body_to_form.
                        Gradients::ENVELOPE;
    }

    /**
     * @access protected
     * @static
     * @var string stores header class toString
     */
    protected static string $header;
    /**
     * @var string stores body string
     */
    protected static string $body;
    /**
     * @var string stores signature class toString
     */
    protected static string $signature;
    /**
     * @static
     * @method create file format with proper extention
     */
    protected static function keyFile(string $_file_name): string
    {
        return $_file_name.".".self::KEYEXT;
    }
    /**
     * @method opens envelope
     * @return array of key parts
     * header, body, signature !
     */
    protected static function openEnvelope(string $_content): array
    {
        return explode(Gradients::ENVELOPE, $_content);
    }
    /**
     * @method brakes body key into blocks
     * @return array of code blocks !
     */
    protected static function brakeContent(string $_content): array
    {
        return explode(Gradients::STING, $_content);
    }

}