<?php

namespace Phr\Certificator\CertificatorBase; 

use Phr\Certificator\CertificatorBase\Macro\Gradients;

/**
 * @abstract
 * Encryption base class
 * 
 * @param key 
 * 
 * @see Encryption 
 */
abstract class EncryptionBase extends Macro\Crips
{   
    /**
     * @access protected
     * @var string
     * @var key
     */
    protected string $key;

    /**
     * 
     * @access protected
     * @var hasAlg
     * Useing hash algorith, default sha256
     * 
     */
    protected static string $hashAlg = IHashVars::SHA256;

    /**
     * @access protected 
     * @var cipher
     * Default AES-128-CBC
     */
    protected static string $sslAlgo = EncryVar::AES128CBD;

    /**
     * @var hmac
     * Set encryption algorithm
     * default: sha256
     */
    protected static string $hmacOpt = EncryVar::HMACOPT;

    // CONSTRUCTOR ***
    public function __construct(string $_key)
    {
        $this->key = $_key;
    }
    /**
     * @method brakes EoId identification string
     * @param string EoId
     * @param string excpected key identificator (AppId / SolutionId ?)
     * @return false|string
     */
    protected static function breakId(string $_eoId, string $_expected_key = null): false|string
    {   
        $b = explode(Gradients::STING, $_eoId);
        if($_expected_key != null)
            if($_expected_key == $b[0]) return $b[1];
        return false;
    }
}