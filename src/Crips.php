<?php

namespace Phr\Certificator;

use Phr\Certificator\CertificatorBase\Macro\Crips as CripsBase;
use Phr\Certificator\ICrips;

final class Crips extends CripsBase implements ICrips {}