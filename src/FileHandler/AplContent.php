<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Certificator\CertificatorBase\Macro\LineFormat;

class AplContent 
{   
    public string $title;

    public array $aplContentRows = [];

    public function __construct(string $_title
        ,AplLine $_apl_content
    ){   
        $this->title = $_title;
        array_push($this->aplContentRows, $_apl_content);
    }
    public function add(AplLine $_apl_content)
    {
        array_push($this->aplContentRows, $_apl_content);
    }
    public function title()
    {
        return(
            [
                LineFormat::line(),
                LineFormat::title($this->title),
                LineFormat::line()
            ]
        );
    }
    public function foot(): string
    {
        return LineFormat::format(GR::CLOSE_FILE);
    }
}