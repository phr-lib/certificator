<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\CertificatorBase\FileHandlerBase;
use Phr\Certificator\FileHandler\ConfigRow;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\ISaveFile as SF;
use Phr\Certificator\Encryption;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Certificator\FileHandler\Errors as ERR;
use Phr\Certificator\FileHandler\FileVars as FV;



class CertHandler extends FileHandlerBase
{   
    private static string $contentSingnature;

    private static string|null $signature = null;

    /**
     * @access public
     * @method create certificate base on 
     * its type.
     * @param object content put in certificate
     * @param string|null filename. If null cert filename
     * is random hash. 
     */
    public function createCertificate(object $_content, string|null $_file_name = null)
    {  
        $cert = new CertCore($_content, self::$settings);
        self::$fullFile = self::$dirPath.$file = parent::file($_file_name);
        $certContentJson = $cert->json();
        self::$contentSingnature = md5($certContentJson);
        switch ( parent::$FV) 
        {   
            case FV::FILE_SFCRY: $coreCertContent = $this->createSimpleCertificate($certContentJson); break;
            case FV::CERT_SFCRT_SIGNED: $coreCertContent = $this->createsshCertificate($certContentJson); break;
            case FV::CERT_SFCRT_PU: $coreCertContent = $this->createPublicSignedSshCertificate($certContentJson); break;
            case FV::CERT_SFCRT_PR: $coreCertContent = $this->createPrivateSignedSshCertificate($certContentJson); break;
            case FV::CERT_SSCRT_PU: $coreCertContent = $this->createSecurePublicCertificate($certContentJson); break;
            case FV::CERT_SSCRT_PR: $coreCertContent = $this->createSecurePrivateCertificate($certContentJson); break;


            default: throw new SaveFileError(ERR::E5315510, '::CREATE::'); break;
        }
        if(isset($coreCertContent))
        $this->certificatorGenerator($coreCertContent);
    }
    /**
     * @method reads certificate based on 
     * its type.
     * @param string filepath
     * @param string|null key. Key is used for
     * ssh based certificates.
     */
    public static function read(string $_cert_file_path, string|null $_key, string|null $_rsa_key): object
    {   
        self::$fullFile = $_cert_file_path;
        if(!file_exists($_cert_file_path)) throw new SaveFileError(ERR::E5305500, $_cert_file_path);
        $certContent = file_get_contents($_cert_file_path);
        if($certContent == false) throw new SaveFileError(ERR::E5305501, $_cert_file_path);
        # Check certificate content
        if(!preg_match('/'.GR::CERT_START.'/', $certContent)) throw new SaveFileError(ERR::E5305515, $_cert_file_path);
        if(!preg_match('/'.GR::CERT_END.'/', $certContent)) throw new SaveFileError(ERR::E5305525, $_cert_file_path);
        # Open certificate
        $certParts = explode(GR::CERT_START, $certContent);
        if(!isset($certParts[0])) throw new SaveFileError(ERR::E5305531, $_cert_file_path);
        self::readHeader($certParts[0]);
        # Get cert content
        if(!isset($certParts[1])) throw new SaveFileError(ERR::E5305555, '::content::');
        $coreContent = explode(GR::CERT_END, $certParts[1]);
        if(!isset($coreContent[1])) throw new SaveFileError(ERR::E5305555, '::signature::');
        # Read signature
        $signature = self::readSignature($coreContent[1]);
        self::$signature = $signature[0];
        self::$contentSingnature = $signature[1];
        if(!isset($coreContent[0])) throw new SaveFileError(ERR::E5305555, '::corecontet::');
        # Swiching reading algo
        switch ( parent::$FV ) 
        {   
            case FV::FILE_SFCRY: $coreContent = self::readsshCertificate($coreContent[0],$_key); break;
            case FV::CERT_SFCRT_SIGNED: $coreContent = self::readsshCertificate($coreContent[0],$_key); break;
            case FV::CERT_SFCRT_PU: $coreContent = self::readPublicSignedCertificate($coreContent[0],$_rsa_key); break;
            case FV::CERT_SFCRT_PR: $coreContent = self::readPrivateSignedSshCertificate($coreContent[0],$_rsa_key); break;
            case FV::CERT_SSCRT_PU: $coreContent = self::readSecurePublicSignedSshCertificate($coreContent[0],$_key, $_rsa_key); break;
            case FV::CERT_SSCRT_PR: $coreContent = self::readSecurePrivateSignedSshCertificate($coreContent[0],$_key, $_rsa_key); break;
            
            default: throw new SaveFileError(ERR::E5305555, '::CERT VARIANT::'); break;
        }
        
        if(self::$contentSingnature !== md5($coreContent))
            throw new SaveFileError(ERR::E5305505, $_cert_file_path);
            
        return json_decode($coreContent);
    }
    /**
     * @access private
     * @method decodes core of certificate
     * @param string certificate core content.
     * @return string decoded content
     */
    private static function readSimpleCertificate(string $_cert_content): string
    {   
        $content = str_replace("\n",'',str_replace(" ",'',$_cert_content));   
        return Encryption::baseDecode(Encryption::decodeHex($content));
    }
    /**
     * @method reads CERT_SFCRT_SIGNED
     * @param string content
     * @param string key
     * @return string ssl decrypted content
     * @throws SaveFileError
     */
    private static function readsshCertificate(string $_cert_content, string|null $_key = null): string
    {   
        if(!$_key) throw new SaveFileError(ERR::E5315001);
        $content = Encryption::decodeHex(parent::contentParse($_cert_content));
        $decrypt = new Encryption($_key);
        return $decrypt->sslDecrypt($content);
    }
    /**
     * @method reads CERT_SFCRT_PU
     * @param string certContetn
     * @param string private rsa path
     * @return string decoded content
     */
    private static function readPublicSignedCertificate(string $_cert_content, string $_rsa_key): string
    {   
        if(!$_rsa_key) throw new SaveFileError(ERR::E5315009);
        $privateKey = file_get_contents($_rsa_key);
        if(!$privateKey) throw new SaveFileError(ERR::E5305501);
        $decrptedKey = Encryption::sslPrivateDecrypt($privateKey, self::$signature);
        return self::readsshCertificate($_cert_content, $decrptedKey);
    }
    /**
     * @method reads CERT_SFCRT_PR
     * @param string certificate
     * @param string public rsa
     * @return string decoded
     * @throws SaveFileError
     */
    private static function readPrivateSignedSshCertificate(string $_cert_content, string $_rsa_key): string
    {   
        if(!$_rsa_key) throw new SaveFileError(ERR::E5315008);
        $publicKey = file_get_contents($_rsa_key);
        if(!$publicKey) throw new SaveFileError(ERR::E5305501);
        $decrptedKey = Encryption::sslPublicDecrypt($publicKey, self::$signature);
        return self::readsshCertificate($_cert_content, $decrptedKey);
    }
    /**
     * @method reads CERT_SSCRT_PU
     * @param string cert content
     * @param string key
     * @param string private rsa path
     * @return string encoded
     * @throws SaveFileError
     */
    private static function readSecurePublicSignedSshCertificate(string $_cert_content, string $_key, string $_private_key): string
    {  
        if(!$_key) throw new SaveFileError(ERR::E5315001);
        if(!$_private_key) throw new SaveFileError(ERR::E5315009);
        $privateKey = file_get_contents($_private_key);
        if(!$privateKey) throw new SaveFileError(ERR::E5305501);
        if(!self::$signature) throw new SaveFileError(ERR::E5315010);
        # Decode key
        $decryptKey = Encryption::sslPrivateDecrypt($privateKey,self::$signature);
        $decrypt = new Encryption($decryptKey);
        $decryptedFirstLevel = $decrypt->sslDecrypt(parent::contentParse($_cert_content));
        if($decryptedFirstLevel == false) throw new SaveFileError(ERR::E5305585);

        return self::readsshCertificate($decryptedFirstLevel, $_key);
    }
    /**
     * @method read CERT_SSCRT_PR
     * @param string certificate content
     * @param string key
     * @param string public rsa
     * @return string decoded
     * @throws SaveFileErrors
     */
    private static function readSecurePrivateSignedSshCertificate(string $_cert_content, string $_key, string $_public_key): string
    {   
        if(!$_key) throw new SaveFileError(ERR::E5315001);
        if(!$_public_key) throw new SaveFileError(ERR::E5315009);
        $publicKey = file_get_contents($_public_key);
        if(!$publicKey) throw new SaveFileError(ERR::E5305501);
        if(!self::$signature) throw new SaveFileError(ERR::E5315010);
        # Dectypt key from signature
        $decryptKey = Encryption::sslPublicDecrypt($publicKey,self::$signature);
        if(!$decryptKey) throw new SaveFileError(ERR::E5305555);
        # Second level decodeing
        $parseContent = parent::contentParse($_cert_content);
        $decrypt = new Encryption($decryptKey);
        $decryptSecondLevel = $decrypt->sslDecrypt($parseContent);

        return self::readsshCertificate($decryptSecondLevel, $_key);
        
    }
    /**
     * @method creates CERT_SFCRT
     * @param string content
     * @return string encrypted content
     */
    private function createSimpleCertificate(string $_content): string
    {   
        return Encryption::encodeHex(Encryption::baseEncode($_content));
    }
    /**
     * @method creates CERT_SFCRT_SIGNED
     * @param string content
     * @return string encoded content
     * @throws SaveFileError
     */
    private function createsshCertificate(string $_content): string
    {   
        if(!self::$settings->key) throw new SaveFileError(ERR::E5315001);
        $encrypt = new Encryption(self::$settings->key);
        $encry = $encrypt->sslEncrypt($_content);
        return Encryption::encodeHex($encry);
    }
    /**
     * @method creates CERT_SFCRT_PU
     * @param string content
     * @return string certificate
     * @throws SaveFileError
     */
    private function createPublicSignedSshCertificate(string $_content): string
    {   
        if(!self::$settings->publicKey) throw new SaveFileError(ERR::E5315008);
        $key = Encryption::generate32bytsKey();
        self::$settings->key = $key;
        $encryContent = $this->createsshCertificate($_content, $key);
        $publicKey = file_get_contents(self::$settings->publicKey);
        if(!$publicKey) throw new SaveFileError(ERR::E5305501);
        $signature = Encryption::sslPublicEncrypt($publicKey, $key);
        if(!$signature) throw new SaveFileError(ERR::E5315010);
        self::$signature = $signature;
        return $encryContent;
    }
    /**
     * @method creates CERT_SFCRT_PR
     * @param string content
     * @return string encoded content
     * @throws SaveFileError
     */
    private function createPrivateSignedSshCertificate(string $_content): string
    {   
        if(!self::$settings->privateKey) throw new SaveFileError(ERR::E5315009);
        $key = Encryption::generate32bytsKey();
        self::$settings->key = $key;
        $encryContent = $this->createsshCertificate($_content, $key);
        $privateKey = file_get_contents(self::$settings->privateKey);
        if(!$privateKey) throw new SaveFileError(ERR::E5305501);
        $signature = Encryption::sslPrivateEncrypt($privateKey, $key);
        self::$signature = $signature;
        if(!$signature) throw new SaveFileError(ERR::E5315010);
        return $encryContent;
    }
    /**
     * @method create CERT_SSCRT_PU
     * @param string content
     * @return string encoded
     * @throws SaveFileError
     */
    private function createSecurePublicCertificate(string $_content): string
    {   
        if(!self::$settings->key) throw new SaveFileError(ERR::E5315001, '::CREATE::');
        if(!self::$settings->publicKey) throw new SaveFileError(ERR::E5315008, '::CREATE::');
        $publicKey = file_get_contents(self::$settings->publicKey);
        if(!$publicKey) throw new SaveFileError(ERR::E5305501, '::CREATE::');
        # Encode fist level
        $encryContent = $this->createsshCertificate($_content);

        # Create encryption key
        $key = Encryption::generate32bytsKey();
        $vertEncryption = new Encryption($key);
        $content = $vertEncryption->sslEncrypt($encryContent);
        # Encode new key into signaure 
        $signature = Encryption::sslPublicEncrypt($publicKey, $key);
        self::$signature = $signature;
        if(!$signature) throw new SaveFileError(ERR::E5315010, '::CREATE::');

        return $content;
    }
    /**
     * @method creates CERT_SSCRT_PR
     * @param string content
     * @return string encypted
     * @throws SaveFileError
     */
    private function createSecurePrivateCertificate(string $_content): string
    {   
        if(!self::$settings->privateKey) throw new SaveFileError(ERR::E5315008, '::CREATE::');
        $privateKey = file_get_contents(self::$settings->privateKey);
        if(!$privateKey) throw new SaveFileError(ERR::E5305501, '::CREATE::');
        # Firs level - encoded with control key
        $encryContent = $this->createsshCertificate($_content);
        # Second level - create new key
        $key = Encryption::generate32bytsKey();
        $vertEncryption = new Encryption($key);
        # Double encode content with new key
        $content = $vertEncryption->sslEncrypt($encryContent);
        # Create signature out of new key
        $signature = Encryption::sslPrivateEncrypt($privateKey, $key);
        self::$signature = $signature;
        if(!$signature) throw new SaveFileError(ERR::E5315010, '::CREATE::');

        return $content;
    }
    
    private function certificatorGenerator(string $_content)
    {   
        self::start();

        self::writeHeader();
        fwrite(self::$FH, GR::CERT_START.PHP_EOL);
        
        if((self::$FV == FV::CERT_SSCRT_PU) || (self::$fileVar == FV::CERT_SSCRT_PR))
        {
            $this->writeCertificateCore($_content,2,25);

        }else $this->writeCertificateCore($_content);

        
        fwrite(self::$FH, GR::CERT_END.PHP_EOL);
        if(self::$signature != null) $this->parseCertificateSignature();
        
        fwrite(self::$FH, GR::SIGNATURE.self::$contentSingnature);

        fclose(self::$FH);
    }
    private function parseCertificateSignature()
    {
        $this->writeCertificateCore(self::$signature, 1, 50);
    }
    /**
     * @method splits and writes encryption string into 
     * rows and colums. Used for writeing certificate
     * core content into a form. 
     * @param string content to write ( encrypted string )
     * @param int colums ( default 5 colums)
     * @param int split ( row colum lenght )
     */
    private function writeCertificateCore(string $_content, int $colums = 5, int $split = 5)
    {   
        $output = str_split($_content, $split);
        $writeCount =  count($output);
        $rows = $writeCount/$colums;
        if(fmod($writeCount, $colums) > 0) $rows++;
        
        $str = 0;
        for($i=0; $i <= round($rows); $i++)
        {   
            if($str >= $writeCount) break;
            for($z=0; $z<$colums; $z++)
            {   
                if($str >= $writeCount) break;
                fwrite(self::$FH, $output[$str]." ");
                $str++;
            }fwrite(self::$FH, PHP_EOL);
        }
    }
    
   
}