<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\Line\ConfLine;
use Phr\Certificator\FileHandler\Line\DblLine;



class ConfigContent 
{
    public array $configRows = [];

    public function __construct(ConfigLine|ConfLine|DblLine $_config_row)
    {
        array_push($this->configRows, $_config_row);
    }
    public function add(ConfigLine|ConfLine|DblLine $_config_row)
    {
        array_push($this->configRows, $_config_row);
    }
}