<?php

namespace Phr\Certificator\FileHandler;

enum Errors 
{
    case E5304000;
    case E5304001;
    case E5304002;
    case E5304011;
    case E5304015;
    case E5304021;
    case E5304501;
    case E5304502;

    case E5305500;
    case E5305501;
    case E5305505;
    case E5305515;
    case E5305525;
    case E5305531;
    case E5305532;

    case E5305555;
    case E5305585;


    case E5315001;
    case E5315008;
    case E5315009;
    case E5315010;
    case E5315510;












    public function code(): int 
    {
        return match ($this) 
        {
            self::E5304000 => 5304000,
            self::E5304001 => 5304001,
            self::E5304002 => 5304002,
            self::E5304011 => 5304011,
            self::E5304015 => 5304015,
            self::E5304021 => 5304021,

            self::E5304501 => 5304501,
            self::E5304502 => 5304502,
            self::E5305500 => 5305500,
            self::E5305501 => 5305501,
            self::E5305505 => 5305505,
            self::E5305515 => 5305515,
            self::E5305525 => 5305525,
            self::E5305531 => 5305531,
            self::E5305532 => 5305532,
            self::E5305555 => 5305555,
            self::E5315001 => 5315001,
            self::E5315008 => 5315008,
            self::E5315009 => 5315009,
            self::E5315010 => 5315010,
            self::E5315510 => 5315510,
            self::E5305585 => 5305585







        };
    }
    public function message(): string 
    {
        return match ($this) 
        {
            self::E5304000 => 'Save file error - unknown file extention',
            self::E5304001 => 'File is not provided',
            self::E5304002 => 'File does not exists',
            self::E5304011 => 'can not read config line',
            self::E5304015 => 'file is not created',
            self::E5304021 => 'malformed db line - .dbl file',
            self::E5304501 => 'file is corupted - precheck',
            self::E5304502 => 'file is corupted - secure check',
            self::E5305500 => 'certificate do not exists',
            self::E5305501 => 'can not read certificate content',
            self::E5305505 => 'invalid content signature',
            self::E5305515 => 'mising flag ----- START CERTIFICETE -----',
            self::E5305525 => 'mising flag ----- END CERTIFICETE -----',
            self::E5305531 => 'can not define certificate header',
            self::E5305532 => 'can not define certificate content',
            self::E5305555 => 'error reading certificate',
            self::E5315001 => 'ssh decription - missing key',
            self::E5315008 => 'ssh decription - missing public key',
            self::E5315009 => 'ssh decription - missing private key',
            self::E5315010 => 'can not create certificate singnature',
            self::E5315510 => 'unknown certificate format',
            self::E5305585 => 'ssl decrypt failed'







        };
    }

}