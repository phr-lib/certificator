<?php

namespace Phr\Certificator\FileHandler\Line;

use Phr\Certificator\CertificatorBase\Macro\Gradients;


class ConfLine 
{
    public string $key;

    public array $rowValues = [];

    public function __construct(string $_key, array $_row_values)
    {
        $this->key = preg_replace('/=/', "_",$_key);
        foreach($_row_values as $value)
        {
            array_push($this->rowValues, preg_replace('/=/', "_",$value));
        }
    }
    public function toString(): string 
    {
        return $this->key.Gradients::CONF_LINE.implode('][', $this->rowValues);
    }
}