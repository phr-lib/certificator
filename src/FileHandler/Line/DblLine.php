<?php

namespace Phr\Certificator\FileHandler\Line;

use Phr\Certificator\CertificatorBase\Macro\Gradients;
use Phr\Certificator\CertificatorBase\Macro\LineFormat;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Certificator\FileHandler\Errors as ERR;
use Phr\Certificator\SaveFileError;



class DblLine 
{
    public string $key;

    public string $value;

    public string $varRow;

    public function __construct(string $_key, string $_value, array $_data_array)
    {   
        if(preg_match('/=/', $_key)) throw new SaveFileError(ERR::E5304021);
        if(preg_match('/\[/', $_key)) throw new SaveFileError(ERR::E5304021);
        if(preg_match('/\]/', $_key)) throw new SaveFileError(ERR::E5304021);
        $this->key = $_key;

        if(preg_match('/=/', $_value)) throw new SaveFileError(ERR::E5304021);
        if(preg_match('/\[/', $_value)) throw new SaveFileError(ERR::E5304021);
        if(preg_match('/\]/', $_value)) throw new SaveFileError(ERR::E5304021);
        if(preg_match('/&/', $_value)) throw new SaveFileError(ERR::E5304021);
        $this->value = $_value;

        $varRow = "";

        foreach($_data_array as $data)
        {   
            if(preg_match('/&/', $data[0])) throw new SaveFileError(ERR::E5304021);
            if(preg_match('/%/', $data[0])) throw new SaveFileError(ERR::E5304021);
            $varRow .= $data[0];
            $varRow .= GR::DBL_VAR_SPR;
            if(preg_match('/&/', $data[1])) throw new SaveFileError(ERR::E5304021);
            if(preg_match('/%/', $data[1])) throw new SaveFileError(ERR::E5304021);
            $varRow .= $data[1];
            $varRow .= "][";
            #var_dump($value);

        }$varRow .= GR::STING_SIGH;
        $this->varRow = $varRow;

    }
    public function toString(): string 
    {
        return ( $this->key.GR::CONF_LINE
            .$this->value.GR::DBL_SPR
            .$this->varRow
        );
    }
    
   
}