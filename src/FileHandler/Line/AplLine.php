<?php

namespace Phr\Certificator\FileHandler\Line;

use Phr\Certificator\CertificatorBase\Macro\Gradients;
use Phr\Certificator\CertificatorBase\Macro\LineFormat;


class AplLine 
{
    public array $statement;

    public string $description;

    public function __construct(array $_statement, string $_description)
    {
        $this->statement = $_statement;
        $this->description = $_description;
    }
    public function statement(): string 
    {
        return LineFormat::statement($this->statement);
    }
    public function description(): string 
    {
        return "/// ".$this->description;
    }
   
}