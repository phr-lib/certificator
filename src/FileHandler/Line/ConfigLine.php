<?php

namespace Phr\Certificator\FileHandler\Line;

use Phr\Certificator\CertificatorBase\Macro\Gradients;


class ConfigLine 
{
    public string $key;

    public string $value;

    public function __construct(string $_key, string|int|bool $_value)
    {
        $this->key = preg_replace('/=/', "_",$_key);
        $this->value = preg_replace('/=/', "_",$_value);
    }
    public function toString(): string 
    {
        return $this->key.Gradients::CONF_LINE.$this->value;
    }
}