<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\CertificatorBase\FileHandlerBase;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\ISaveFile as SF;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\Errors as ERR;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;



class ConfigHandler extends FileHandlerBase
{   
    public function create(
        ConfigContent|AplContent $_confg_content,
        string $_file_name = null
    ): void {
        if($_confg_content instanceof ConfigContent) $this->writeNewConfigFile($_confg_content, $_file_name);
        if($_confg_content instanceof AplContent) $this->writeAplFile($_confg_content, $_file_name);
    }
    
    private function writeNewConfigFile(
        ConfigContent $_confg_content,
        string $_file_name = null
    ): void {

        self::$fullFile = self::$dirPath.$file = parent::file($_file_name);

        self::start();

        self::writeHeader();

        
        
        foreach($_confg_content->configRows as $row)
        {   
            fwrite(self::$FH, $row->toString(). PHP_EOL);
        }
        fclose(self::$FH);

        if(!file_exists(self::$fullFile)) throw new SaveFileError(ERR::E5304015);
        
    }
    private function writeAplFile(
        AplContent $_apl_content,
        string $_file_name = null
    ): void {

        self::$fullFile = self::$dirPath.$file = parent::file($_file_name);
        
        self::start();

        self::writeHeader();

        fwrite(self::$FH, PHP_EOL);
        
        foreach($_apl_content->title() as $titleLine)
        {
            fwrite(self::$FH, $titleLine. PHP_EOL);
        }
        fwrite(self::$FH, PHP_EOL);
        

        foreach($_apl_content->aplContentRows as $row)
        {   
            fwrite(self::$FH, PHP_EOL);
            fwrite(self::$FH, $row->description(). PHP_EOL);
            fwrite(self::$FH, $row->statement(). PHP_EOL);
        }
        fwrite(self::$FH, PHP_EOL);
        fwrite(self::$FH, $_apl_content->foot().PHP_EOL);
        fclose(self::$FH);

        if(!file_exists(self::$fullFile)) throw new SaveFileError(ERR::E5304015);
        
    }
    public static function readConfigFile(string $_file_to_read): array
    {    
        # Read file content line by line
        $content = self::readFileByLine($_file_to_read);
        $c = [];
        foreach($content as $line)
        {   
            if(!preg_match('/=/', $line)) throw new SaveFileError(ERR::E5304011);
            $s = explode('=', $line);
            $c[trim($s[0])] = trim($s[1]);
        }
        return $c;       
    }
    public static function readConfFile(string $_file_to_read): array
    {
        $content = self::readFileByLine($_file_to_read);
        $c = [];
        foreach($content as $line)
        {   
            if(preg_match('/=/', $line))
            {
                $lineBlocks = explode('=', $line);
                
                if(isset($lineBlocks[1])) $list = $lineBlocks[1];
                if(preg_match('/\]/', $list))
                {   
                    $listArray = explode('][', $list);
                    $listResult = [];
                    foreach($listArray as $listItem)
                    {   
                        array_push($listResult, trim($listItem));
                    }
                    if(isset($lineBlocks[0])) $listKey = $lineBlocks[0];

                    $c[trim($listKey)] = $listResult;

                    return $c;
                    
                } throw new SaveFileError(ERR::E5304011);       
            } throw new SaveFileError(ERR::E5304011);
        }
    }
    public static function readDblFile(string $_file_to_read)
    {   
        $content = [];
        $contentLines = self::readFileByLine($_file_to_read);
        foreach($contentLines as $line)
        {
            if(preg_match('/'.GR::CONF_LINE.'/', $line))
            {
                $c = explode(GR::CONF_LINE, $line);
                if(preg_match('/'.GR::DBL_SPR.'/', $c[1]))
                {
                    $lt = explode(GR::DBL_SPR, $c[1]);
                    $p = explode('][', $lt[1]);
                    $innerVars = [];
                    foreach($p as $variabile)
                    {   
                        if(preg_match('/'.GR::DBL_VAR_SPR.'/', $variabile))
                        {   
                            $vp = explode(GR::DBL_VAR_SPR, $variabile);
                            $innerVars[$vp[0]] = $vp[1];
                        }
                    }
                    $innerArray =  [$lt[0], $innerVars];
                }else throw new SaveFileError(ERR::E5304011);
                $content[$c[0]] = $innerArray;
            }else throw new SaveFileError(ERR::E5304011);
        } return $content;
    }
    
}