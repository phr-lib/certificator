<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\FileHandler\FileVars as FV;

class FileSettings 
{   
    public FV $FV;

    public string $solutionId;

    public string $applicationId;

    public string|null $key;

    public string|null $publicKey;

    public string|null $privateKey;


    public function __construct(
        string $_file_var,
        string $_solution_id,
        string $_app_id,
        string|null $_key = null,
        string|null $_public_key = null,
        string|null $_private_key = null
    ){  
        $this->FV = self::getFileVar($_file_var);
        $this->solutionId = $_solution_id;
        $this->applicationId = $_app_id;
        $this->key = $_key;
        $this->publicKey = $_public_key;
        $this->privateKey = $_private_key;

    }
    public static function getFileVar(string $_file_var): FV
    {
        switch ($_file_var) 
        {
            case FV::FILE_APL->code(): return FV::FILE_APL; break;
            case FV::FILE_SFCRY->code(): return FV::FILE_SFCRY; break;
            case FV::FILE_CRYLIST->code(): return FV::FILE_CRYLIST; break;
            case FV::CERT_SFCRT->code(): return FV::CERT_SFCRT; break;
            case FV::CERT_SFCRT_SIGNED->code(): return FV::CERT_SFCRT_SIGNED; break;
            case FV::CERT_SFCRT_PU->code(): return FV::CERT_SFCRT_PU; break;
            case FV::CERT_SFCRT_PR->code(): return FV::CERT_SFCRT_PR; break;
            case FV::CERT_SSCRT_PU->code(): return FV::CERT_SSCRT_PU; break;
            case FV::CERT_SSCRT_PR->code(): return FV::CERT_SSCRT_PR; break;
            case FV::FILE_DBL->code(): return FV::FILE_DBL; break;
        }
    }
}