<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\ISaveFile;


enum FileVars 
{   
    private const CONFIG_EXT = 'conf';

    private const CONF_EXT = 'config';

    private const CERT_EXT = 'cert';

    private const APL_EXT = 'apl';

    private const DBL_EXT = 'dbl';


    case FILE_APL;
    case FILE_SFCRY;
    case FILE_CRYLIST;
    case CERT_SFCRT;
    case CERT_SFCRT_SIGNED;
    case CERT_SFCRT_PU;
    case CERT_SFCRT_PR;
    case CERT_SSCRT_PU;
    case CERT_SSCRT_PR;
    case FILE_DBL;

    public function code(): string 
    {
        return match ($this) {
            self::FILE_APL => ISaveFile::FILE_APL,
            self::FILE_SFCRY => ISaveFile::FILE_SFCRY,
            self::FILE_CRYLIST => ISaveFile::FILE_CRYLIST,
            self::CERT_SFCRT => ISaveFile::CERT_SFCRT,
            self::CERT_SFCRT_SIGNED => ISaveFile::CERT_SFCRT_SIGNED,
            self::CERT_SFCRT_PU => ISaveFile::CERT_SFCRT_PU,
            self::CERT_SFCRT_PR => ISaveFile::CERT_SFCRT_PR,
            self::CERT_SSCRT_PU => ISaveFile::CERT_SSCRT_PU,
            self::CERT_SSCRT_PR => ISaveFile::CERT_SSCRT_PR,
            self::FILE_DBL => ISaveFile::FILE_DBL
        };
    }
    public function ext(): string 
    {
        return match ($this) {
            self::FILE_APL => self::APL_EXT,
            self::FILE_SFCRY => self::CONFIG_EXT,
            self::FILE_CRYLIST => self::CONF_EXT,
            self::CERT_SFCRT => self::CERT_EXT,
            self::CERT_SFCRT_SIGNED => self::CERT_EXT,
            self::CERT_SFCRT_PU => self::CERT_EXT,
            self::CERT_SFCRT_PR => self::CERT_EXT,
            self::CERT_SSCRT_PU => self::CERT_EXT,
            self::CERT_SSCRT_PR => self::CERT_EXT,
            self::FILE_DBL => self::DBL_EXT
        };
    }
}

