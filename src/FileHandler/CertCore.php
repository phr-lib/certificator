<?php

namespace Phr\Certificator\FileHandler;

use Phr\Certificator\FileHandler\FileSettings;

class CertCore 
{   
    public string $solutionId;

    public string $applicationId;

    public object $content;

    public function __construct(object $_content, FileSettings $_settings)
    {
        $this->content = $_content;
        $this->solutionId = $_settings->solutionId;
        $this->applicationId = $_settings->applicationId;

    }

    public function json()
    {
        return json_encode($this);
    }
}
