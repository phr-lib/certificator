<?php

namespace Phr\Certificator;

use Phr\Certificator\Encry\HashAlgo;

/**
 * 
 * @final Phr\Certificator\Encryption
 * @method hashIt
 * @param string content
 * @param Phr\Certificator\Encry\HashAlgo
 * 
 * PHP 8.2 or above
 * 
 * @category Authorisation encryption, keys, fingerprints
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 * 
 * @see Encryption 53 00 000
 * 
 */
interface IHash 
{   
    /**
     * @static
     * @access public
     * @method hashEncodeContent
     * @param string content
     * @param HashAlgo
     * @return string
     * 
     */
    public static function hashEncodeContent( string | int $_content_to_hash, HashAlgo $_hash_algo ): string;

    public const MD2 = "md2";

    public const MD4 = "md4";

    public const MD5 = "md5";

    public const SHA1 = "sha1";

    public const SHA256 = "sha256";

    public const SHA384 = "sha384";

    public const SHA512 = "sha512";

    public const ripemd128 = "ripemd128";

    public const ripemd160 = "ripemd160";

    public const ripemd256 = "ripemd256";

    public const ripemd320 = "ripemd320";

    public const whirlpool = "whirlpool";

    public const tiger128 = "tiger128,3";

    public const tiger192 = "tiger192,3";

    public const tiger160 = "tiger160,4";

    public const SNEFRU = "snefru";

    public const GOST = "gost";

    public const ADLER = "adler32";


}