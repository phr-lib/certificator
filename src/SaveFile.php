<?php

namespace Phr\Certificator;

use Phr\Certificator\CertificatorBase\FileHandlerBase;
use Phr\Certificator\FileHandler\ConfigHandler;
use Phr\Certificator\FileHandler\ConfigRow;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\CertHandler;
use Phr\Certificator\FileHandler\FileVars as FV;
use Phr\Certificator\FileHandler\Errors as ERR;


/**
 * @final
 * 
 * @see interface ISaveFile
 */
final class SaveFile extends FileHandlerBase implements ISaveFile 
{   
    public static function read(string $_file_to_read, string|null $_key = null, string|null $_rsa_key = null): array|object
    {   
        switch (parent::fileExtention($_file_to_read)) 
        {
            case FV::FILE_SFCRY->ext(): return ConfigHandler::readConfigFile($_file_to_read); break;
            case FV::FILE_CRYLIST->ext(): return FileHandler\ConfigHandler::readConfFile($_file_to_read);  break;
            case FV::FILE_APL->ext(): return self::readConfigFile($_file_to_read); break;
            case FV::CERT_SFCRT->ext(): return FileHandler\CertHandler::read($_file_to_read, $_key, $_rsa_key); break;  
            case FV::FILE_DBL->ext(): return ConfigHandler::readDblFile($_file_to_read); break;      
            default: throw new SaveFileError(ERR::E5304000); break;
        }
    }
    public function create(ConfigContent|AplContent $_confg_content, string $_file_name = null): void 
    {   
        self::createPath();
        $handler = new ConfigHandler(self::$dirPath, self::$settings);
        $handler->create($_confg_content, $_file_name);
    }
    
    public function createCertificate(object $_content, string|null $_file_name = null): void
    {   
        $handler = new FileHandler\CertHandler(self::$dirPath, self::$settings);
        $handler->createCertificate($_content, $_file_name);
    }


    public function createConfigFile(ConfigContent|AplContent $_confg_content, string $_file_name = null): void 
    {   
        self::createPath();
        $handler = new ConfigHandler(self::$dirPath, self::$settings);
        $handler->create($_confg_content, $_file_name);
    }
}