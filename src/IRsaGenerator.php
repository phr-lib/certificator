<?php

namespace Phr\Certificator;

use Phr\Certificator\FileHandler\ConfigContent;


/**
 * 
 * @final Phr\Certificator\Encry\RsaKeyGenerator
 * 
 * PHP 8.2 or above
 * 
 * @category Authorisation encryption, keys, fingerprints
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 * 
 * @see Encryption
 * 
 */
interface IRsaGenerator 
{   
    public const VERSION = '1.0.1';
     /**
     * 
     * @access public
     * @method create new rsa key
     * pairs. 
     * 
     */
    public function createKeyPairs(): void;
    /**
     * 
     * @method saves keys into file.
     * @param string directory path
     * @param string|null filename
     * Default file name is public.pem/private.pem 
     * 
     */
    public function saveKeys( string $_file_path, string|null $_file_name = null): void;
    /**
     * @return string public key.
     */
    public function publicKey(): string;
    /**
     * @return string private key
     */
    public function privateKey(): string;
    /**
     * @method changes default public filename
     * @param string new filename
     */
    public function publicFileName(string $_new_file_name): void;
    /**
     * @method changes default filename
     * @param string new filename
     */
    public function privateFileName(string $_new_file_name): void; 
    
}