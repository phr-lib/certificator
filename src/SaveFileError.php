<?php

namespace Phr\Certificator;

use Phr\Certificator\FileHandler\Errors as ERR;

class SaveFileError extends \Exception 
{
    public function __construct(ERR $ERR, string $_text = '')
    {
        $this->code = $ERR->code();
        $this->message = $ERR->message() .' '.$_text;
    }
}