<?php

namespace Phr\Certificator\Encry;

use Phr\Certificator\IRsaGenerator;
use Phr\Certificator\CertificatorBase\EncryVar;

abstract class KeyGeneratorBase
{   
    protected static array $config;

    protected static string $alg;

    protected static int $bits;

    protected const FILE_EXE = '.pem';

    protected static string $filenamePublic = 'public';
    
    protected static string $filenamePrivate = 'private'; 
    
    public function __construct(string $_ssl_algo = EncryVar::SSL_SHA512, int $_bits = 4096)
    {   
        self::$config = array(

            "digest_alg" => $_ssl_algo,
            "private_key_bits" => $_bits,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,

        );
    }
}

final class RsaKeyGenerator extends KeyGeneratorBase implements IRsaGenerator
{   
    private string $privateKey;

    private string $publicKey;

   
    public function createKeyPairs(): void 
    {   
        $res = openssl_pkey_new( self::$config );

        openssl_pkey_export($res, $privKey);
        
        $this->privateKey = $privKey;

        $pubKey = openssl_pkey_get_details($res);

        $this->publicKey = $pubKey["key"];
    }

    
    public function saveKeys( string $_file_path, string|null $_file_name = null): void 
    {   
        if( $_file_name == null)
        {
            $FileNamePrivate = self::$filenamePrivate.self::FILE_EXE;
            $FileNamePublic = self::$filenamePublic.self::FILE_EXE;
        }else
        {
            $FileNamePrivate = $_file_name.'.'.self::$filenamePrivate.self::FILE_EXE;
            $FileNamePublic =  $_file_name.'.'.self::$filenamePublic.self::FILE_EXE;
        }
        

        file_put_contents($_file_path.DIRECTORY_SEPARATOR.$FileNamePrivate, $this->privateKey );

        file_put_contents($_file_path.DIRECTORY_SEPARATOR.$FileNamePublic, $this->publicKey );
    }
    public function publicKey(): string 
    {
        return $this->publicKey;
    }
    public function privateKey(): string 
    {
        return $this->privateKey;
    }
    public function publicFileName(string $_new_file_name): void 
    {
        self::$filenamePublic = $_new_file_name;
    }
    public function privateFileName(string $_new_file_name): void 
    {
        self::$filenamePrivate = $_new_file_name;
    }
}