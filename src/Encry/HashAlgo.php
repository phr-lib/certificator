<?php

namespace Phr\Certificator\Encry;

use Phr\Certificator\IHash;

enum HashAlgo: string 
{
    case MD2 = IHash::MD2;

    case MD4 = IHash::MD4;

    case MD5 = IHash::MD5;

    case SHA1 = IHash::SHA1;

    case SHA256 = IHash::SHA256;

    case SHA384 = IHash::SHA384;

    case SHA512 = IHash::SHA512;

    case ripemd128 = IHash::ripemd128;

    case ripemd160 = IHash::ripemd160;

    case ripemd256 = IHash::ripemd256;

    case ripemd320 = IHash::ripemd320;

    case whirlpool = IHash::whirlpool;

    case tiger128 = IHash::tiger128;

    case tiger192 = IHash::tiger192;

    case tiger160 = IHash::tiger160;

    case SNEFRU = IHash::SNEFRU;

    case GOST = IHash::GOST;

    case ADLER = IHash::ADLER;

}