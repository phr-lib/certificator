<?php

namespace Phr\Certificator\Encry;

use Phr\Certificator\CertificatorBase\EncryptionBase;

class SslEncryption extends EncryptionBase
{
    /**
     * @method encrypt
     * Open ssl enryption
     */
    public function sslEncrypt( string $_content_to_encrypt ): string
    {   
        return $this->runEncryptionSequence($_content_to_encrypt, $this->key, self::$sslAlgo, self::$hmacOpt );
    }
    private function runEncryptionSequence(string $_content_to_encrypt, string $key, string $sslAlgo, string $hmac_opt): string
    {   
        $ivlen = openssl_cipher_iv_length($sslAlgo);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($_content_to_encrypt, $sslAlgo, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac($hmac_opt, $ciphertext_raw, $key, $as_binary=true);
        return (string)$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    /**
     * @method decrypt
     * Open ssl enryption
     */
    public function sslDecrypt( string $_content_to_decrypt ): false | string
    {              
        return $this->runDecryptSequnce($_content_to_decrypt, $this->key , self::$sslAlgo, self::$hmacOpt);
    }
    public function runDecryptSequnce( string $_content_to_decrypt, string $key, string $sslAlgo, string $hmac_opt ): false | string
    {
        $c = base64_decode($_content_to_decrypt);
        $ivlen = openssl_cipher_iv_length($sslAlgo);
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original = openssl_decrypt($ciphertext_raw, $sslAlgo, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac($hmac_opt, $ciphertext_raw, $key, $as_binary=true);
        if (hash_equals($hmac, $calcmac))
            return $original;
        return false;
    }
}