<?php

namespace Phr\Certificator\Encry;


/**
 * 
 * Create instance of encoder/decoder
 * 
 * PHP version 8.2 or above
 * 
 * @category Authentication
 * @author Grega Lipovšček
 * @license https://lab.ortus.si
 * @see Encryption class
 * 
 * @link https://ortus.si
 * 
*/
class Fernet 
{   
    const VERSION = "\x80";

    /**
     * @access private
     * @var encryptionKey
     */
    private string $encryptionKey;
    /**
     * @var signingKey
     */
    private string $signingKey;

    
    // CONSTRUCTOR ***
    /**
     * Create instance of encoder/decoder
     * @param key
     * @throws Exception
     */
    public function __construct(string $_key)
    {
        if (!extension_loaded('openssl') && !extension_loaded('mcrypt')) 
        {
            throw new \Exception('No library extentions found !');
        }

        $key = self::urlsafeB64Decode($_key);
        
        if (strlen($key) != 32) 
        {
            throw new \Exception('Incorrect key - must be base64 encoded 32-byte');
        }
        $this->signingKey = substr($key, 0, 16);
        $this->encryptionKey = substr($key, 16);
    }

    /**
     * @method encode
     * @param string $message
     */
    public function encode(string $_mesasge)
    {
        $iv = $this->getIV();

        $pad = 16 - (strlen($_mesasge) % 16);
        
        $_mesasge .= str_repeat(chr($pad), $pad);

        if (function_exists('openssl_encrypt')) 
        {
            $ciphertext = base64_decode(openssl_encrypt($_mesasge, 'aes-128-cbc', $this->encryptionKey, OPENSSL_ZERO_PADDING, $iv));
        } else 
        {
            $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->encryptionKey, $_mesasge, 'cbc', $iv);
        }
        $signing_base = self::VERSION . pack('NN', 0, $this->getTime()) . $iv . $ciphertext;
        $hash = hash_hmac('sha256', $signing_base, $this->signingKey, true);

        return self::urlsafeB64Encode($signing_base . $hash);
    }

    /**
     * @method decode
     * @param string token
     * @param int ttl - time until is valid
     * Decodes message
     */
    public function decode(string $_token, $_ttl = null)
    {
        $raw = self::urlsafeB64Decode($_token);
        $hash = substr($raw, -32);
        if (!is_string($hash)) {
            throw new \Exception("Invalid token !");
        }
        $signing_base = substr($raw, 0, -32);
        $expected_hash = hash_hmac('sha256', $signing_base, $this->signingKey, true);

        if (!hash_equals($hash, $expected_hash)) {
            throw new \Exception("Invalid signature");
        }

        $parts = unpack('Cversion/Ndummy/Ntime', substr($signing_base, 0, 9));
        if (chr($parts['version']) != self::VERSION) {
            throw new \Exception("Token version mismatched");
        }

        if ($_ttl != null) {
            if ($parts['time'] + $_ttl < $this->getTime()) {
                throw new \Exception("Token is expired");
            }
        }

        $iv = substr($signing_base, 9, 16);
        $ciphertext = substr($signing_base, 25);

        if (function_exists('openssl_decrypt')) {
            $message = openssl_decrypt(base64_encode($ciphertext), 'aes-128-cbc', $this->encryptionKey, OPENSSL_ZERO_PADDING, $iv);
        } else {
            $message = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->encryptionKey, $ciphertext, 'cbc', $iv);
        }

        $pad = ord($message[strlen($message) - 1]);
        if (substr_count(substr($message, -$pad), chr($pad)) != $pad) {
            throw new \Exception("Token is malformed");
        }

        return substr($message, 0, -$pad);
    }

    /**
     * @access protected
     * @method getIV
     */
    protected function getIV()
    {
        return random_bytes(16);
    }

    protected function getTime()
    {
        return time();
    }

    /**
     * @method urlsafeB64Encode
     * @param $input
     * @return base64string
     * 
     */
    public static function urlsafeB64Encode(string $input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
    /**
     * @static
     * @method generateKey
     * @return string of 64 encoded random bytes
     */
    static public function generateKey(int $_key_lenght = 32): string
    {
        return self::urlsafeB64Encode(random_bytes($_key_lenght));
    }
    /**
     * @static
     * @method urlsafeB64Decode
     * @param string $input
     * @return string decoded 64 string
     */
    public static function urlsafeB64Decode(string $_input): string
    {
        $rem = strlen($_input) % 4;
        if ($rem) {
            $pad = 4 - $rem;
            $_input .= str_repeat('=', $pad);
        }
        return base64_decode(strtr($_input, '-_', '+/'));
    }
}