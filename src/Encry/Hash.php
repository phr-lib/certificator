<?php

namespace Phr\Certificator\Encry;

use Phr\Certificator\IHash;
use Phr\Certificator\Encry\HashAlgo;

class Hash implements IHash
{   
    /**
     * @static
     * @access public
     * @method hashEncodeContent
     * @param contentToHash
     * @return string
     * 
     */
    public static function hashEncodeContent( string | int $_content_to_hash, HashAlgo $_hash_algo ): string
    {   
        return hash( $_hash_algo->value, $_content_to_hash );
    }
 
}