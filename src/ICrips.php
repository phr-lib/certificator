<?php

namespace Phr\Certificator;

use Phr\Certificator\CertificatorBase\IHashVars;


/**
 * 
 * @final Phr\Certificator\Crips
 * @param string key
 * 
 * PHP 8.2 or above
 * 
 * @category Authorisation encryption, keys, fingerprints
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 * 
 * @see Encryption
 * 
 */
interface ICrips
{   
    /**
     * 
     * @method code
     * @param int char lenght of block
     * Generates random code. Default code lenght is 5
     * @return int block code
     */
    public static function code( int $_code_lenght = 5): int;
    /**
     * @method generateKeyBlock
     * @param int key lenght
     * @return string block key
     */
    public static function generateKeyBlock( int $keyLenght = 5): string;
    /**
     * @method generateUniquId
     * @return uniquId 63deaece8c034 - 63deaece8c035 - 63deaece8c036 - 63deaece8c037 - 63deaece8c038
     */
    public static function generateUniquId(): string;
    /**
     * 
     * @method newGuid
     * Creates new guid
     */
    public static function newGuid(): string;
    /**
     * 
     * @method generateFingerPrint
     * @return string finger print 
     * @param int finger print lenght | default to 5 blocks
     * Example: P2y::O0s::F8h::C8y::X4k
     * 
     */
    public static function generateFingerPrint( int $fingerPrintLenght = 5): string;
    /**
     * @method password
     * @param string password
     * @return string password hash
     */
    public static function password( string $_unencrypted_password ): string;
    /**
     * @method veryfy password
     * @param string password
     * @param string password hash
     * @return bool true if password is correct
     */
    public static function password_vertify( string $_unencrypted_password, string $hash): bool;
    /**
     * @method encrypt
     * @param string content
     * @param string|int vertifier code
     * @return string enryped string
     */
    public static function encrypt( string $unencrypted, string|int $vertifier): string;
    /**
     * @method generateUniqueKeyId
     * @param string prefix
     * @param bool true for extention
     * @return string uniquePrefixId
     */
    public static function generateUniqueKeyId( string $_pfeFix = "", bool $_t = true ): string;
} 