<?php

namespace Phr\Certificator\CertKey;

/**
 * 
 * @final Phr\Certificator\CertKey\BlockKey
 * @param string key path
 * 
 * @category Authorisation, keyblock
 * @author Grega Lipovscek
 * @license https://lab.ortus.si
 * 
 * @link grega.lipovscek@ortus.si
 */
interface ICertKey
{   
    public const VERSION = '1.0.1';
    /**
     * @method generates new selected key
     */
    public function generate(): void;
    /**
     * @method saves key to file
     * @param string keyname - if is null, it generates md5
     * hash name !
     */
    public function save(string $_key_name = null): bool;
    /**
     * @static
     * @method loads key from file, opens it and 
     * brakes it to keyparts
     * @return array of code blocks !
     * @throws Exception
     */
    public static function load(string $_file_path): array;
}


