<?php

namespace Phr\Certificator\CertKey;

use Phr\Certificator\CertificatorBase\CertKeyModel;
use Phr\Certificator\CertificatorBase\Macro\KeyHeader;
use Phr\Certificator\CertificatorBase\Macro\KeySignature;
use Phr\Certificator\CertificatorBase\Macro\Crips;
use Phr\Certificator\CertificatorBase\Macro\Gradients;


final class BlockKey extends CertKeyModel implements ICertKey
{   
    private string $fileName;

    public function generate(): void
    {
        $this->newTimeStamp();
        $header = new KeyHeader($this->timestamp);
        self::$header = $header->toString();
        $this->formBody(Crips::generateKeyBlock());
        $signature = new KeySignature($this->timestamp);
        self::$signature = $signature->toString();
    }
    public function save(string $_key_name = null): bool
    {   
        $this->fileName = md5(rand());
        $keyFile = $this->keyPath."/";
        $keyFile .= ($_key_name == null) ? self::keyFile($this->fileName): self::keyFile($_key_name);
        return file_put_contents($keyFile, self::$header.self::$body.self::$signature);
        
    }
    public function getFileName(): string 
    {
        return $this->fileName;
    }
    public static function load(string $_file_path): array
    {   
        $file = self::keyFile($_file_path);
        if(!file_exists($file)) throw new \Exception("missing file");
        $content = file_get_contents($file);
        $envelope = self::openEnvelope($content);
        return self::brakeContent($envelope[1]);
    }
}
